package com.teams.nfc_test.data.remote.services

import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse
import com.teams.nfc_test.utils.Urls
import retrofit2.Response
import retrofit2.http.*

interface IAuthService {



    @GET(Urls.LATEST)
    suspend fun getExchangeRate(): Response<ExchangeRateResponse>


}