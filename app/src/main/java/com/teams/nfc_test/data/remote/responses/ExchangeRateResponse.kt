package com.teams.nfc_test.data.remote.responses


data class ExchangeRateResponse(

    val rates: Currency


)

data class Currency(val USD : String)


