package com.teams.nfc_test.data.repository

import com.teams.nfc_test.data.remote.controller.Resource
import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse
import com.teams.nfc_test.model.Transaction

interface DataSource {

    suspend fun getExchangeRate(): Resource<ExchangeRateResponse>
    suspend fun insertTransaction(transaction: Transaction)
    suspend fun getLastRecordForBalance(balanceNumber: Int): List<Transaction>
}