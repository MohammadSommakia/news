package com.teams.nfc_test.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.teams.nfc_test.data.local.repository.dao.TransactionDao
import com.teams.nfc_test.model.Transaction

@Database(
    entities = [Transaction::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun transactionDao(): TransactionDao
}