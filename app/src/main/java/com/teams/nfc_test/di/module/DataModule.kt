package com.teams.nfc_test.di.module

import com.teams.nfc_test.data.repository.DataRepository
import com.teams.nfc_test.data.repository.DataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataModule {
    @Binds
    @Singleton
    abstract fun provideDataRepository(dataRepository: DataRepository): DataSource
}