package com.teams.nfc_test.di.module

import androidx.lifecycle.ViewModel
import com.teams.nfc_test.presentation.read.ReadViewModel
import com.teams.nfc_test.presentation.write.WriteViewModel
import com.teams.nfc_test.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

// Because of @Binds, ViewModelModule needs to be an abstract class

@Module
abstract class ViewModelModule {

// Use @Binds to tell Dagger which implementation it needs to use when providing an interface.
    @Binds
    @IntoMap
    @ViewModelKey(ReadViewModel::class)
    abstract fun bindReadViewModel(viewModel: ReadViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WriteViewModel::class)
    abstract fun bindWriteViewModel(viewModel: WriteViewModel): ViewModel
}