package com.teams.nfc_test.utils

class Const {

    companion object {
        const val DATABASE_NAME: String = "NFC_BALANCE"
        //Network constants
        const val TIMEOUT_CONNECT = 60L   //In seconds
        const val TIMEOUT_READ = 60L   //In seconds
        const val TIMEOUT_WRITE = 60L   //In seconds

        const val ALIAS_KEY_AES128 = "key_aes_128"
        const val ALIAS_KEY_2KTDES = "key_2ktdes"
        const val ALIAS_KEY_2KTDES_ULC = "key_2ktdes_ulc"
        const val ALIAS_DEFAULT_FF = "alias_default_ff"
        const val ALIAS_KEY_AES128_ZEROES = "alias_default_00"
        const val EXTRA_KEYS_STORED_FLAG = "keys_stored_flag"

        /**
         * KEY_APP_MASTER key used for encrypting the data.
         */
        const val KEY_APP_MASTER = "This is my key"

        /**
         * Ultralight First User Memory Page Number.
         */
        const val PAGE_TO_READ_WRITE =1


        /**
         * Constant for permission
         */
        const val TOAST_PRINT = 'd'
        const val TOAST = 't'
        const val PRINT = 'n'

        const val SUBTRACT = "SUBTRACT"


    }
}