package com.teams.nfc_test.presentation.read

enum class ReadAction {

    SUBTRACT_2, SUBTRACT_5, RESET
}