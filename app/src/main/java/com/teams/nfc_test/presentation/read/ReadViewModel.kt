package com.teams.nfc_test.presentation.read

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.teams.nfc_test.data.repository.DataRepository
import com.teams.nfc_test.utils.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ReadViewModel  @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {

     val action = MutableLiveData<Event<ReadAction>>()
     val validation = MutableLiveData<Event<ReadValidation>>()



    fun subtract2Clicked()
    {
        // check the stored value in db
        launch {
          val lastUpdatedBalance =    withContext(Dispatchers.IO) {
              dataRepository.getLastRecordForBalance(1)
          }
            if (lastUpdatedBalance.isEmpty() || lastUpdatedBalance[0].oldBalanceUSD >1)
                action.value = Event(ReadAction.SUBTRACT_2)
            else
                validation.value = Event(ReadValidation.INVALID_SUBTRACT_2)

        }
    }

    fun subtract5Clicked()
    {

        // check the stored value in db
        launch {
            val lastUpdatedBalance =   withContext(Dispatchers.IO) {
               dataRepository.getLastRecordForBalance(2)
            }

            if (lastUpdatedBalance.isEmpty() || lastUpdatedBalance[0].oldBalanceUSD > 4)
                    action.value = Event(ReadAction.SUBTRACT_5)
                else
                    validation.value = Event(ReadValidation.INVALID_SUBTRACT_5)
        }
    }

    fun resetClicked()
    {
        action.value = Event(ReadAction.RESET)
    }


}