package com.teams.nfc_test.presentation.write

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.teams.nfc_test.R
import com.teams.nfc_test.data.remote.controller.ErrorManager
import com.teams.nfc_test.data.remote.controller.Resource
import com.teams.nfc_test.data.remote.responses.ExchangeRateResponse
import com.teams.nfc_test.databinding.ActivityWriteBinding
import com.teams.nfc_test.model.Transaction
import com.teams.nfc_test.presentation.base.BaseActivity
import com.teams.nfc_test.utils.Const
import com.teams.nfc_test.utils.EventObserver
import com.teams.nfc_test.utils.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class WriteActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    private lateinit var binding: ActivityWriteBinding

    override val tag = "WriteActivity"
    private val bytesToWrite = ByteArray(2)

    private val viewModel: WriteViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(WriteViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityWriteBinding.inflate(layoutInflater)
        binding.viewModel = viewModel
        setContentView(binding.root)
        setupObservers()
        viewModel.getExchangeRateResponse()
    }

    override fun onNewIntent(intent: Intent?) {
        mStringBuilder.delete(0, mStringBuilder.length)
        // we will  store the data  in the first 2 bytes

        viewModel.getTheLastTransactionFor2Values()
        super.onNewIntent(intent)
    }

    private fun setupObservers() {
        viewModel.response.observe(
            this, EventObserver
                (object : EventObserver.EventUnhandledContent<Resource<ExchangeRateResponse>> {
                override fun onEventUnhandledContent(t: Resource<ExchangeRateResponse>) {
                    when (t) {
                        is Resource.Loading -> {
                            // here we can show progress bar
                            showMessage(
                                getString(R.string.loading_exchange_rate),
                                Const.TOAST
                            )
                        }
                        is Resource.DataError -> {
                            showMessage(
                                ((t.response as ErrorManager).error),
                                Const.TOAST
                            )
                        }
                        is Resource.Exception -> {
                            showMessage(
                                t.response as String,
                                Const.TOAST
                            )
                        }
                        else -> {
                            // do nothing
                        }
                    }
                }
            })
        )
        viewModel.result.observe(
            this, EventObserver
                (object : EventObserver.EventUnhandledContent<List<Transaction>> {
                override fun onEventUnhandledContent(t: List<Transaction>) {
                    if (intent?.getIntExtra(Const.SUBTRACT, 0) == 2) {
                        // first we filter for transaction 1
                        // if is empty that means the value is 10
                        // else we get the last value from database and subtract 2
                        val transaction1 = t.filter {
                            it.balanceNumber == 1
                        }
                        if (transaction1.isEmpty())
                            bytesToWrite[0] = 8
                        else
                            bytesToWrite[0] = (transaction1[0].newBalanceUSD - 2).toByte()

                        // same for transaction2 but here we write the last value to NFC directly
                        val transaction2 = t.filter {
                            it.balanceNumber == 2
                        }
                        if (transaction2.isEmpty())
                            bytesToWrite[1] = 20.toByte()
                        else
                            bytesToWrite[1] = (transaction2[0].newBalanceUSD).toByte()
                        //write to NFC
                        handleLogic(intent, bytesToWrite)
                        if (writeDone) {
                            val oldBalance = bytesToWrite[1].toInt()
                            val newBalance = bytesToWrite[1].toInt() + 5
                            val newBalanceInEUR = bytesToWrite[1].toInt() * viewModel.exchangeRate

                            viewModel.insertTransaction(
                                Transaction(
                                    2,
                                    oldBalance,
                                    newBalance,
                                    newBalanceInEUR,
                                    viewModel.exchangeRate.toString()
                                )
                            )
                        }
                    } else
                        if (intent?.getIntExtra(Const.SUBTRACT, 0) == 5) {
                            val transaction1 = t.filter {
                                it.balanceNumber == 1
                            }
                            if (transaction1.isEmpty())
                                bytesToWrite[0] = 10.toByte()
                            else
                                bytesToWrite[0] = (transaction1[0].newBalanceUSD).toByte()

                            // same for transaction2 but here we write the last value to NFC directly
                            val transaction2 = t.filter {
                                it.balanceNumber == 2
                            }
                            if (transaction2.isEmpty())
                                bytesToWrite[1] = 15.toByte()
                            else
                                bytesToWrite[1] = (transaction2[0].newBalanceUSD - 5).toByte()
                            //write to NFC
                            handleLogic(intent, bytesToWrite)
                            if (writeDone) {
                                val oldBalance = bytesToWrite[1].toInt()
                                val newBalance = bytesToWrite[1].toInt() + 5
                                val newBalanceInEUR =
                                    bytesToWrite[1].toInt() * viewModel.exchangeRate

                                viewModel.insertTransaction(
                                    Transaction(
                                        2,
                                        oldBalance,
                                        newBalance,
                                        newBalanceInEUR,
                                        viewModel.exchangeRate.toString()
                                    )
                                )
                            }
                        } else   //reset
                        {
                            bytesToWrite[0] = 10.toByte()
                            bytesToWrite[1] = 20.toByte()

                            //write to NFC
                            handleLogic(intent, bytesToWrite)
                            if (writeDone) {
                                var oldBalance = bytesToWrite[1].toInt()
                                var newBalance = 10
                                var newBalanceInEUR =
                                    bytesToWrite[0].toInt() * viewModel.exchangeRate

                                viewModel.insertTransaction(
                                    Transaction(
                                        1,
                                        oldBalance,
                                        newBalance,
                                        newBalanceInEUR,
                                        viewModel.exchangeRate.toString()
                                    )
                                )

                                oldBalance = bytesToWrite[1].toInt()
                                newBalance = 20
                                newBalanceInEUR =
                                    bytesToWrite[1].toInt() * viewModel.exchangeRate

                                viewModel.insertTransaction(
                                    Transaction(
                                        2,
                                        oldBalance,
                                        newBalance,
                                        newBalanceInEUR,
                                        viewModel.exchangeRate.toString()
                                    )
                                )
                            }
                        }
                }
            })
        )
    }
}