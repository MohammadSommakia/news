package com.teams.nfc_test.presentation.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nxp.nfclib.CardType
import com.nxp.nfclib.KeyType
import com.nxp.nfclib.NxpNfcLib
import com.nxp.nfclib.desfire.DESFireFactory
import com.nxp.nfclib.desfire.DESFireFile.StdDataFileSettings
import com.nxp.nfclib.desfire.EV1ApplicationKeySettings
import com.nxp.nfclib.desfire.IDESFireEV1
import com.nxp.nfclib.exceptions.NxpNfcLibException
import com.nxp.nfclib.interfaces.IKeyData
import com.nxp.nfclib.ntag.INTag
import com.nxp.nfclib.ntag.NTagFactory
import com.nxp.nfclib.utils.NxpLogUtils
import com.nxp.nfclib.utils.Utilities
import com.teams.nfc_test.R
import com.teams.nfc_test.utils.Const
import com.teams.nfc_test.utils.EnumKeyType
import com.teams.nfc_test.utils.KeyInfoProvider
import com.teams.nfc_test.utils.SampleAppKeys
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Cipher
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.IvParameterSpec


open class BaseActivity : AppCompatActivity() {


    open val tag = "BaseActivity"

    private var packageKey = "a81f56f88e23e83d4617c23686cd9d22"

    lateinit var readData: ByteArray

    private var objKey2KtDesUlc: IKeyData? = null
    private var objKey2KtDes: IKeyData? = null
    private var objKeyAes128: IKeyData? = null
    private var defaultFfKey: ByteArray? = null
    private var defaultZeroesKey: IKeyData? = null

    private lateinit var libInstance: NxpNfcLib

    private var bytesKey: ByteArray? = null

    private var cipher: Cipher? = null

    private var iv: IvParameterSpec? = null

    private val appId = byteArrayOf(0x12, 0x00, 0x00)


    var mStringBuilder = StringBuilder()
    var writeDone = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        libInstance = NxpNfcLib.getInstance()
        try {
            libInstance.registerActivity(this, packageKey)
        } catch (ex: NxpNfcLibException) {
            Toast.makeText(this, ex.message, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            // do nothing added to handle the crash if any
        }

        initializeKeys()

        /* Initialize the Cipher and init vector of 16 bytes with 0xCD */
        initializeCipherInitVector()
    }



     fun handleLogic(intent: Intent?, byteArray: ByteArray)  {

        var type = CardType.UnknownCard
        try {
            type = libInstance.getCardType(intent)
            if (type == CardType.UnknownCard) {
                showMessage(getString(R.string.UNKNOWN_TAG), Const.PRINT)
            }
        } catch (ex: NxpNfcLibException) {
            ex.message?.let { showMessage(it, Const.TOAST) }
        }
        when (type) {

            CardType.NTag216 ->
                try {
                    return ntagCardLogic(
                        NTagFactory.getInstance().getNTAG216(libInstance.customModules),byteArray
                    )
                } catch (t: Throwable) {
                    showMessage(getString(R.string.unknown_Error_Tap_Again), Const.TOAST_PRINT)
                }

            CardType.DESFireEV1 ->
                try {
                    return  desfireEV1CardLogic(
                        DESFireFactory.getInstance().getDESFire(
                            libInstance.customModules
                        ),byteArray
                    )
                } catch (t: Throwable) {
                    showMessage(getString(R.string.unknown_Error_Tap_Again), Const.TOAST_PRINT)
                }


            else -> {
                }
        }

    }


    private fun initializeKeys() {
        val infoProvider: KeyInfoProvider = KeyInfoProvider.getInstance(applicationContext)
        val sharedPrefs = getPreferences(Context.MODE_PRIVATE)
        val keysStoredFlag = sharedPrefs.getBoolean(Const.EXTRA_KEYS_STORED_FLAG, false)
        if (!keysStoredFlag) {
            //Set Key stores the key in persistent storage, this method can be called only once if key for a given alias does not change.
            val ulc24Keys = ByteArray(24)
            System.arraycopy(
                SampleAppKeys.KEY_2KTDES_ULC,
                0,
                ulc24Keys,
                0,
                SampleAppKeys.KEY_2KTDES_ULC.size
            )
            System.arraycopy(
                SampleAppKeys.KEY_2KTDES_ULC,
                0,
                ulc24Keys,
                SampleAppKeys.KEY_2KTDES_ULC.size,
                8
            )
            infoProvider.setKey(
                Const.ALIAS_KEY_2KTDES_ULC,
                EnumKeyType.EnumDESKey,
                ulc24Keys
            )
            infoProvider.setKey(
                Const.ALIAS_KEY_2KTDES,
                EnumKeyType.EnumDESKey,
                SampleAppKeys.KEY_2KTDES
            )
            infoProvider.setKey(
                Const.ALIAS_KEY_AES128,
                EnumKeyType.EnumAESKey,
                SampleAppKeys.KEY_AES128
            )
            infoProvider.setKey(
                Const.ALIAS_KEY_AES128_ZEROES,
                EnumKeyType.EnumAESKey,
                SampleAppKeys.KEY_AES128_ZEROS
            )
            infoProvider.setKey(
                Const.ALIAS_DEFAULT_FF,
                EnumKeyType.EnumMifareKey,
                SampleAppKeys.KEY_DEFAULT_FF
            )
            sharedPrefs.edit().putBoolean(Const.EXTRA_KEYS_STORED_FLAG, true).apply()
            //If you want to store a new key after key initialization above, kindly reset the flag EXTRA_KEYS_STORED_FLAG to false in shared preferences.
        }
        objKey2KtDesUlc =
            infoProvider.getKey(Const.ALIAS_KEY_2KTDES_ULC, EnumKeyType.EnumDESKey)
        objKey2KtDes = infoProvider.getKey(Const.ALIAS_KEY_2KTDES, EnumKeyType.EnumDESKey)
        objKeyAes128 = infoProvider.getKey(Const.ALIAS_KEY_AES128, EnumKeyType.EnumAESKey)
        defaultZeroesKey =
            infoProvider.getKey(Const.ALIAS_KEY_AES128_ZEROES, EnumKeyType.EnumAESKey)
        defaultFfKey = infoProvider.getMifareKey(Const.ALIAS_DEFAULT_FF)
    }

    private fun initializeCipherInitVector() {

        /* Initialize the Cipher */
        try {
            cipher = Cipher.getInstance("AES/CBC/NoPadding")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        }
        /* set Application Master Key */bytesKey = Const.KEY_APP_MASTER.toByteArray()

        /* Initialize init vector of 16 bytes with 0xCD. It could be anything */
        val ivSpec = ByteArray(16)
        Arrays.fill(ivSpec, 0xCD.toByte())
        iv = IvParameterSpec(ivSpec)
    }

    /**
     * DESFire Pre Conditions.
     *
     *
     * PICC Master key should be factory default settings, (ie 16 byte All zero
     * Key ).
     *
     *
     */
    private fun desfireEV1CardLogic(desFireEV1: IDESFireEV1, writeData: ByteArray) {
        writeDone = false
        desFireEV1.reader.connect()
        desFireEV1.reader.timeout = 2000
        val fileSize = 100
//        val data = byteArrayOf(0x11, 0x11, 0x11, 0x11, 0x11)
        val fileNo = 0
        mStringBuilder.append(getString(R.string.Card_Detected)).append(
            desFireEV1.type.tagName
        )
        mStringBuilder.append("\n\n")
        try {
            mStringBuilder.append(getString(R.string.Selecting_PICC))
            mStringBuilder.append("\n\n")
            desFireEV1.selectApplication(0)
            mStringBuilder.append(getString(R.string.PICC_selection_success))
            mStringBuilder.append("\n\n")
            mStringBuilder.append(getString(R.string.Auth_with_default_key))
            mStringBuilder.append("\n\n")
            desFireEV1.authenticate(
                0, IDESFireEV1.AuthType.Native, KeyType.THREEDES,
                objKey2KtDes
            )
            mStringBuilder.append(getString(R.string.Authentication_status_true))
            mStringBuilder.append("\n\n")
            mStringBuilder.append(getString(R.string.Creating_application))
            mStringBuilder.append("\n\n")
            val appsetbuilder = EV1ApplicationKeySettings.Builder()
            val appsettings = appsetbuilder.setAppKeySettingsChangeable(
                true
            ).setAppMasterKeyChangeable(true)
                .setAuthenticationRequiredForFileManagement(false)
                .setAuthenticationRequiredForDirectoryConfigurationData(
                    false
                ).setKeyTypeOfApplicationKeys(
                    KeyType.TWO_KEY_THREEDES
                ).build()
            desFireEV1.createApplication(appId, appsettings)
            mStringBuilder.append(getString(R.string.App_creation_success)).append(
                Utilities.dumpBytes(appId)
            )
            mStringBuilder.append("\n\n")
            desFireEV1.selectApplication(appId)
            desFireEV1.createFile(
                fileNo, StdDataFileSettings(
                    IDESFireEV1.CommunicationType.Plain,
                    0.toByte(),
                    0.toByte(),
                    0.toByte(),
                    0.toByte(),
                    fileSize
                )
            )
            desFireEV1.authenticate(
                0, IDESFireEV1.AuthType.Native, KeyType.TWO_KEY_THREEDES,
                objKey2KtDes
            )
            mStringBuilder.append(getString(R.string.Writing_data_to_tag))
            mStringBuilder.append("\n\n")
            mStringBuilder.append(getString(R.string.Data_to_write)).append(
                Utilities.dumpBytes(readData)
            )
            mStringBuilder.append("\n\n")
            if (writeData.isNotEmpty())
            desFireEV1.writeData(0, 0, readData)
            mStringBuilder.append(getString(R.string.Data_written_successfully))
            writeDone = true
            mStringBuilder.append("\n\n")
            readData = desFireEV1.readData(0, 0, 5)
            mStringBuilder.append(getString(R.string.Data_read_from_the_card)).append(
                Utilities.dumpBytes(desFireEV1.readData(0, 0, 5))
            )
            mStringBuilder.append("\n\n")
            showMessage(mStringBuilder.toString(), Const.PRINT)
            desFireEV1.reader.close()

        } catch (e: java.lang.Exception) {
            writeFailedMessage()
            mStringBuilder.append(e.message)
            showMessage(mStringBuilder.toString(), Const.PRINT)
        }
    }

    /**
     * Ntag IO Operations.
     *
     * @param tag object
     */
    private fun ntagCardLogic(tag: INTag, writeData: ByteArray)  {
        writeDone = false
        if (!tag.reader.isConnected)
        tag.reader.connect()
        mStringBuilder.append(getString(R.string.Card_Detected)).append(tag.type.tagName)
        mStringBuilder.append("\n\n")
            try {
                if (writeData.isNotEmpty()) {
                    mStringBuilder.append(getString(R.string.Writing_data_at_page_number)).append(
                        Const.PAGE_TO_READ_WRITE
                    ).append("...")
                    mStringBuilder.append("\n\n")
                    mStringBuilder.append(getString(R.string.Data_to_write)).append(
                        Utilities.dumpBytes(writeData)
                    )
                    mStringBuilder.append("\n\n")
                    tag.write(1, writeData)
                    mStringBuilder.append(
                        getString(R.string.Written_4_bytes_of_data_at_page_no)
                    ).append(1).append(
                        ":"
                    ).append(Utilities.dumpBytes(writeData))
                    writeDone = true
                    mStringBuilder.append("\n\n")
                    showMessage(mStringBuilder.toString(), Const.PRINT)
                }
                else
                     this.readData = tag.read(1)
            } catch (e: java.lang.Exception) {
                writeFailedMessage()
                mStringBuilder.append(e.message)
                showMessage(mStringBuilder.toString(), Const.PRINT)
            }

    }

    protected open fun showMessage(str: String, operationType: Char) {
        when (operationType) {
            Const.TOAST -> Toast.makeText(this, str, Toast.LENGTH_SHORT)
                .show()
            Const.PRINT -> {
                NxpLogUtils.i(tag, getString(R.string.Dump_data) + str)
            }
            Const.TOAST_PRINT -> {
                Toast.makeText(
                    this, """
     
     $str
     """.trimIndent(), Toast.LENGTH_SHORT
                ).show()
                NxpLogUtils.i(
                    tag, """
     
     $str
     """.trimIndent()
                )
            }
            else -> {
            }
        }
    }

    private fun writeFailedMessage() {
        mStringBuilder.append(getString(R.string.Unable_to_perfom_to_operation))
        mStringBuilder.append("\n\n")
    }

}