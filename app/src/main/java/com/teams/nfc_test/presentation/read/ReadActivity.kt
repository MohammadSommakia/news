package com.teams.nfc_test.presentation.read

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.teams.nfc_test.R
import com.teams.nfc_test.databinding.ActivityReadBinding
import com.teams.nfc_test.presentation.base.BaseActivity
import com.teams.nfc_test.presentation.write.WriteActivity
import com.teams.nfc_test.utils.Const
import com.teams.nfc_test.utils.EventObserver
import com.teams.nfc_test.utils.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class ReadActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    private lateinit var binding: ActivityReadBinding
    private lateinit var writeIntent: Intent

    override val tag = "ReadActivity"

    private val viewModel: ReadViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(ReadViewModel::class.java)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityReadBinding.inflate(layoutInflater)
        binding.viewModel = viewModel
        setContentView(binding.root)
        writeIntent = Intent(this,WriteActivity::class.java)
        setupObservers()
        initViews()
    }

    private fun initViews() {
    }

    override fun onNewIntent(intent: Intent?) {
        mStringBuilder.delete(0, mStringBuilder.length)
        handleLogic(intent,ByteArray(0))
        // we are assuming that the data stored in the first 2 bytes
        if (readData.isNotEmpty()) {
            binding.firstBalanceValue.visibility = View.VISIBLE
            binding.secondBalanceValue.visibility = View.VISIBLE
            binding.firstBalanceValue.text = getString(
                R.string.first_balance_value_is,readData[0].toInt().toString())
            binding.secondBalanceValue.text =getString(
                R.string.second_balance_value_is,readData[1].toInt().toString())
        }
        super.onNewIntent(intent)
    }
    private fun setupObservers() {

        viewModel.validation.observe(
            this, EventObserver
                (object : EventObserver.EventUnhandledContent<ReadValidation> {
                override fun onEventUnhandledContent(t: ReadValidation) {
                    when(t)
                    {
                         ReadValidation.INVALID_SUBTRACT_2 ->
                         {
                             showMessage("can not perform subtract operation for first value", Const.TOAST)
                         }
                         ReadValidation.INVALID_SUBTRACT_5 ->
                         {
                             showMessage("can not perform subtract operation for second value", Const.TOAST)
                         }
                    }
                }
            })
        )
        viewModel.action.observe(
            this, EventObserver
                (object : EventObserver.EventUnhandledContent<ReadAction> {
                override fun onEventUnhandledContent(t: ReadAction) {
                    when(t)
                    {
                        ReadAction.SUBTRACT_2 ->
                        {
                            writeIntent.putExtra(Const.SUBTRACT,2)
                        }
                        ReadAction.SUBTRACT_5 ->
                        {
                            writeIntent.putExtra(Const.SUBTRACT,5)
                        }
                        ReadAction.RESET ->
                        {
                            writeIntent.putExtra(Const.SUBTRACT,0)
                        }
                    }
                    startActivity(writeIntent)
                }
            })
        )
    }
}